local movement_timeout = sumo.movement_timeout --for how long players may run (and jump a little extra too)
local S = core.get_translator("sumo")
local huds = {}

local function num_players(pl_tbl)
    cnt = 0
    for _,_ in pairs(pl_tbl) do
        cnt = cnt + 1
    end
    return cnt
end

local function sumo_show_controls_hud(p_name)

    player = core.get_player_by_name(p_name)
    if not player then return end
    huds[p_name]= {}
    huds[p_name].controls = player:hud_add({
        hud_elem_type   = "image",
        number          = 0xE6482E,
        position        = { x = 1, y = 1},
        offset          = {x = -6,   y = -6},
        text            = "sumo_hudcontrols.png",
        alignment       = {x = -1, y = -1},
        scale           = {x = 3, y = 3},
    })
end

local function sumo_remove_controls_hud(p_name)
    player = core.get_player_by_name(p_name)
    if not player then return end
    if huds[p_name] ~= nil and huds[p_name].controls ~= nil then
        player:hud_remove(huds[p_name].controls)
        huds[p_name].controls = nil
    end
end

local function sumo_set_attribute_lives(arena, pl_name)
    local player = core.get_player_by_name(pl_name)
    if player then
        local lives = arena.players[pl_name].lives
        local tag = " "
        for i=1,lives do
            tag = tag .. "♡ "
        end
        player:set_nametag_attributes({
        text = tag,
        color = {a=255, r=255, g=255, b=255}
    })
    end
end


local function send_message(arena,num_str)
    arena_lib.HUD_send_msg_all("title", arena, num_str, 1,nil,0xE6482E)
    --arena_lib.HUD_send_msg_all(HUD_type, arena, msg, <duration>, <sound>, <color>)
end

arena_lib.on_load("sumo", function(arena)

    --send controls statement
    for pl_name, stats in pairs(arena.players) do
        ---core.log('First: '..dump(pl_name).. " is "..type(pl_name))
        local player = core.get_player_by_name(pl_name)
        sumo.invincible[pl_name] = true
        sumo_show_controls_hud(pl_name)
        sumo_set_attribute_lives(arena, pl_name)



        arena.players[pl_name].lives = arena.lives
        local player = core.get_player_by_name(pl_name)
        local pos = player:get_pos()

        --core.log('Second: '..dump(pl_name).. " is "..type(pl_name))
        core.after(.2,function(pl_name,pos)
            --core.log('Third: '..dump(pl_name).. " is "..type(pl_name))
            local player = core.get_player_by_name(pl_name)
            if player and arena_lib.is_player_in_arena(pl_name, 'sumo') then
                player:move_to(pos)
            end
        end,pl_name,pos)

    end

    --countdown timer, give item at appropriate time
    send_message(arena,'3')
    core.after(1, function(arena)

        send_message(arena,'2')
        core.after(1, function(arena)
            send_message(arena,'1')
            core.after(1, function(arena)
                arena_lib.HUD_send_msg_all("title", arena, S("Fight!"), 1,nil,0x71AA34)

                local item = ItemStack("sumo:pushstick")


                for pl_name, stats in pairs(arena.players) do
                    sumo.invincible[pl_name] = false
                    local player = core.get_player_by_name(pl_name)
                    player:get_inventory():set_stack("main", 1, item)


                end



            end, arena)

        end, arena)

    end, arena)

end)

--this is necessary beacuse it is required by arena_lib for timed games
arena_lib.on_time_tick('sumo', function(arena)


    --handle speed boosts
    for pl_name, stats in pairs(arena.players) do
        local player = core.get_player_by_name(pl_name)
        local keys = player:get_player_control()
        local set_run_speed = false -- a marker to tell whether to set to the run speed or not



        if stats.run_timeout > 0 then
            arena.players[pl_name].run_timeout = arena.players[pl_name].run_timeout - 1
        end

        if keys.aux1 and stats.run_timeout <= 0 then
            set_run_speed = true
            if stats.running == false then
                arena.players[pl_name].running = true
                arena.players[pl_name].run_start_time = core.get_gametime()
            end
            local start_run_time = arena.players[pl_name].run_start_time
            local current_time = core.get_gametime()


            if current_time - start_run_time > movement_timeout then
                set_run_speed = false
                arena.players[pl_name].running = false
                arena.players[pl_name].run_timeout = 5
            end
        end

        if set_run_speed == true and not sumo.invincible[pl_name] then
            player:set_physics_override({
                speed = arena.speed * 1.4,
                jump = arena.jump * 1.3,
            })
        else
            player:set_physics_override({
                speed = arena.speed,
                jump = arena.jump,
            })
            arena.players[pl_name].running = false
        end



        --handle messages
        if arena.in_game and not arena.in_celebration then
            local c = 0x71AA34
            if arena.current_time < 60 then
                c = 0xF4B41B
            end
            if arena.current_time < 10 then
                c = 0xE6482E
            end
            local message = " " .. S("Time Left: @1", arena.current_time)
            if arena.players[pl_name].run_timeout > 0 then
                message = S("Run Timeout: @1", arena.players[pl_name].run_timeout) .. message
            end
            if arena.players[pl_name].running == true then
                message = S("Running...").." ".. message
            end
            message = message.." ".."♡ x ".." " ..arena.players[pl_name].lives
            if sumo.invincible[pl_name] == true then
                message = S("Invincible")
                c = 0xE6482E
            end
            if arena.current_time < arena.initial_time - 1 then
                arena_lib.HUD_send_msg('hotbar', pl_name, message, 1,nil,c)
            end

        end

    end






end)

function sumo.kill_player(arena,pl_name)
    if sumo.invincible[pl_name] == true then return end
    sumo.invincible[pl_name] = true
    local player = core.get_player_by_name(pl_name)
    local inv = player:get_inventory()
    local taken = inv:remove_item("main", ItemStack("sumo:pushstick"))

    arena.players[pl_name].lives = arena.players[pl_name].lives - 1

    -- keep track of kills. We will save a kill log
    local xc_name
    if arena.players[pl_name].last_punched and sumo.get_s_time() - arena.players[pl_name].last_punched.time < 5 then
        if sumo.debug then
            core.chat_send_all("punch time: "..sumo.get_s_time() - arena.players[pl_name].last_punched.time)
            core.chat_send_all("punch type: "..arena.players[pl_name].last_punched.punch_type)
            core.chat_send_all("punch name: "..arena.players[pl_name].last_punched.hitter)
        end
        xc_name = arena.players[pl_name].last_punched.hitter
        local punch_type = arena.players[pl_name].last_punched.punch_type
        table.insert(arena.players[pl_name].kill_log, 1, {killer = xc_name, punch_type = punch_type})
    else
        table.insert(arena.players[pl_name].kill_log, 1, {killer = nil, punch_type = "fall"})
    end


    if arena.players[pl_name].lives == 0 then
        if player then
            sumo_remove_controls_hud(pl_name)
            arena_lib.remove_player_from_arena(pl_name, 1, xc_name)
            arena_lib.HUD_hide('hotbar', pl_name)
        end
    else
        arena_lib.HUD_send_msg("title", pl_name,S("You Died! Lives: @1", arena.players[pl_name].lives), 2,nil,0xFF1100)
        core.after(2,function(pl_name)
            if arena_lib.is_player_in_arena(pl_name, 'sumo') and not(arena_lib.is_player_spectating(pl_name)) then
                local arena = arena_lib.get_arena_by_player(pl_name)
                if arena.in_game == true then
                    arena_lib.HUD_send_msg("title", pl_name,S("Fight!"), 2,nil,0x00FF00)
                    local player = core.get_player_by_name(pl_name)
                    if player then
                        sumo_set_attribute_lives(arena, pl_name)
                        arena_lib.teleport_onto_spawner(player, arena)
                        player:get_inventory():set_stack("main", 1, ItemStack("sumo:pushstick"))
                        core.after(3,function(pl_name)
                            sumo.invincible[pl_name] = false

                        end,pl_name)
                    end
                end
            end
        end,pl_name)
        core.sound_play('sumo_elim', {
            to_player = pl_name,
            gain = 2.0,
        })
        player:move_to(arena.jail_pos, false)

    end
    player:set_hp(20)
    return 0
end


core.register_on_player_hpchange(function(player, hp_change,reason)
    local pl_name = player:get_player_name()
    if arena_lib.is_player_in_arena(pl_name, 'sumo') then
        local arena = arena_lib.get_arena_by_player(pl_name)
        local hp = player:get_hp()
        --exclude some unwanted possibilities
        if arena.in_celebration then --protect winners from damage
            return 0
        end
        if reason.type ~= "node_damage" then return 0 end
        if arena_lib.is_player_spectating(pl_name) then return 0 end -- dont let spectators die, or get the pushstick, only run this if spectate mode is available.
        if sumo.invincible[pl_name] then return 0 end --protects players from dying twice in a row
        if hp + hp_change <= 0 then --dont ever kill players, but if a damage *would* kill them, then eliminate them, and set their health back to normal

            sumo.kill_player(arena,pl_name)
            return 0
        else
            return hp_change --if it would not kill players then apply damage as normal
        end


    else
        return hp_change
    end


end, true)


--if the game times out
arena_lib.on_timeout('sumo', function(arena)
    local winner_names = {}
    for p_name, p_stats in pairs(arena.players) do
        sumo_remove_controls_hud(p_name)
        table.insert(winner_names, p_name)
    end
    --arena_lib.load_celebration('sumo', arena, winner_names)
    arena_lib.force_arena_ending('sumo', arena,'timeout')

end)



arena_lib.on_death('sumo', function(arena, p_name, reason)
    arena.players[p_name].lives = arena.players[p_name].lives - 1
    if arena.players[p_name].lives == 0 then

        local player = core.get_player_by_name(p_name)
        if player then
            arena_lib.remove_player_from_arena(p_name, 1)

            arena_lib.HUD_hide('hotbar', p_name)
        end
    else
        arena_lib.HUD_send_msg("title", p_name,S("You Died! Lives: @1", arena.players[p_name].lives), 2,nil,0xFF1100)
        local player = core.get_player_by_name(p_name)
        core.sound_play('sumo_elim', {
            to_player = p_name,
            gain = 2.0,
        })
        if player then
            sumo_set_attribute_lives(arena, p_name)
            arena_lib.teleport_onto_spawner(player, arena)
        end

    end
end)


arena_lib.on_celebration('sumo', function(arena, winner_name)
    for p_name, p_stats in pairs(arena.players) do
        local player = core.get_player_by_name(p_name)
        sumo_remove_controls_hud(p_name)
    end
    arena_lib.HUD_hide('hotbar', arena)
    local winner_stats = arena.players[winner_name]
    if winner_stats.lives == arena.lives then
        sumo.award(winner_name, "sumo:cant_touch_this")
    end
end)

arena_lib.on_quit('sumo', function(arena, pl_name, is_forced)
    sumo_remove_controls_hud(pl_name)
    arena_lib.HUD_hide('hotbar', pl_name)
end)


arena_lib.on_eliminate('sumo', function(arena, p_name, xc_name, player_properties)
    core.sound_play('sumo_lose', {
        to_player = p_name,
        gain = 2.0,
    })

    local count = 0
    local sound = 'sumo_elim'
    for p_name,data in pairs(arena.players) do
        count = count + 1
    end
    if count == 1 then
        sound = 'sumo_win'
    end

    local player = core.get_player_by_name(p_name)

    for p_name, stats in pairs(arena.players) do
        core.sound_play(sound, {
            to_player = p_name,
            gain = 2.0,
        })
    end

    -- reversi
    -- achievements tracking

    if sumo.debug and player_properties.kill_log then
        core.chat_send_all("kill log: "..dump(player_properties.kill_log))
    end

    -- first, was the last player eliminated with a swap
    if player_properties.kill_log and 
        player_properties.kill_log[1] and 
        player_properties.kill_log[1].killer and
        player_properties.kill_log[1].punch_type == "swap" then

        sumo.award(player_properties.kill_log[1].killer, "sumo:reversi")
    end
    
    -- sole executioner
    -- Check if there is a kill log with at least one kill. Only works at the end of the game.

    if sumo.debug then
        core.chat_send_all("Length of arena.players: "..#arena.players)
        core.chat_send_all("Length of arena.past_present_players: "..#arena.past_present_players)
    end

    if num_players(arena.past_present_players) >= 3 and num_players(arena.players) == 1 and player_properties.kill_log and #player_properties.kill_log > 0 then
        -- Assume the first kill’s killer is our reference

        local all_same_killer = true

        local first_killer = player_properties.kill_log[1].killer

        if first_killer then
             -- Iterate over the entire kill log
            for i = 2, #player_properties.kill_log do
                -- If any killer doesn't match the first killer, break out
                if player_properties.kill_log[i].killer ~= first_killer then
                    all_same_killer = false
                    break
                end
            end

            -- If, after checking them all, they were the same,
            -- then award that single “sole executioner”
            if all_same_killer then
                sumo.award(first_killer, "sumo:sole_executioner")
                if arena.players[first_killer].lives == arena.lives then
                    sumo.award(first_killer, "sumo:grand_executioner")
                end
            end
        end
    end


end)

--remove stick if in inv when joinplayer
core.register_on_joinplayer(function(player)
	local inv = player:get_inventory()
	local stack = ItemStack("sumo:pushstick")
	local taken = inv:remove_item("main", stack)
end)




-- kill players who are in water/killer air on the globalstep instead of in the on_tick

core.register_globalstep(function(dtime)

    for _,player in ipairs(core.get_connected_players()) do
        local pl_name = player:get_player_name()
        if arena_lib.is_player_in_arena(pl_name, "sumo") and not(arena_lib.is_player_spectating(pl_name)) then
            local arena = arena_lib.get_arena_by_player(pl_name)
            if not(arena.in_queue == true) and not (arena.in_loading == true) and not(arena.in_celebration == true) and arena.enabled == true then
                local stats = arena.players[pl_name]

                local pos = player:get_pos()
                local node = core.get_node(pos).name
                -- kill players found in the killer water or air
                if node == "sumo:player_killer_air" or
                    node == "sumo:player_killer_water_source" or
                    node == "sumo:player_killer_water_flowing" or
                    node == "sumo:fullclip" then

                        sumo.kill_player(arena,pl_name)
                end

                if sumo.invincible[pl_name] then
                    core.add_particle({
                        pos = vector.add(player:get_pos(), vector.new(math.random(-.3,.3),math.random(1,2),math.random(-.3,.3))),
                        velocity = {x=0, y=1, z=0},
                        acceleration = {x=0, y=5, z=0},
                        expirationtime = 1,
                        size = 2,
                        collisiondetection = true,
                        collision_removal = true,
                        object_collision = false,
                        vertical = false,
                        texture = "invincible.png",
                        glow = 3,
                    })
                end
            end
        end
    end
end)
